//
// Created by cristiana on 16.06.2018.
//

#include "ReadWrite.h"

ReadWrite::ReadWrite(){}

std::mutex ReadWrite::lockContFile;

Client* ReadWrite::processMainFile(std::string introPin, std::string introSerial){
    std::ifstream infile("../Database/MainFile");
    if (infile) {
        std::string infoCard;
       // std::getline(infile, infoCards);
        //std::cout<<infoCards;


        while (std::getline(infile, infoCard)) {
            if (infoCard.length() == 0) {
                std::cout << ">>> Problem with retrieving initial data..." << std::endl;
                infile.close();
                return nullptr;
            }
            std::vector<std::string> info;
            info = Tokenizer::tokenize(infoCard,'~');
            Card card;
            card.pin = info.at(0);
            card.serial = info.at(1);
            card.idCont = info.at(2);
            card.idClient = info.at(3);
            //std::cout << card.pin << " " << card.serial << " " << introPin << " " << introSerial << std::endl;
            if(introPin == card.pin && introSerial == card.serial) {
                infile.close();
                return processInfoCards(card);
            }

             if(infile.eof()) {
                std::cout<<">>> Invalid pin or card serial..."<<std::endl;
                infile.close();
                return nullptr;
            }

        }

    }
    else {
        return nullptr;
    }
}

Client* ReadWrite::processInfoCards(Card card){
   // for(Card token: cards)
        //std::cout<<"---------pin:"<<token.pin<<" serial: "<<token.serial<<" idcont: "<<token.idCont<<" idclient: "<<token.idClient<<std::endl;
    const std::string basePathClient = "../Database/Client/";
    const std::string basePathCont = "../Database/Cont/";
    Client *client;

    //std::cout<<basePathCont+card.idCont<<" "<<card.idCont.size()<<std::endl;
    std::ifstream fileClient(basePathClient + card.idClient);
    std::ifstream fileCont(basePathCont + card.idCont);
    // std::cout<<fileCont.tellg()<<std::endl;
    if (fileClient) {
        std::string id;
        std::string firstName;
        std::string lastName;
        std::string birthday;
        std::string address;
        std::string email;
        std::string phone;
        std::getline(fileClient, id);
        std::getline(fileClient, firstName);
        std::getline(fileClient, lastName);
        std::getline(fileClient, birthday);
        std::getline(fileClient, address);
        std::getline(fileClient, email);
        std::getline(fileClient, phone);
        if (id.length() == 0 || firstName.length() == 0 || lastName.length() == 0 || birthday.length() == 0 || address.length() == 0 || email.length() == 0 || phone.length() == 0) {
            std::cout << ">>> One(more) information(s) is(are) incomplete..." << std::endl;
            fileClient.close();
            fileCont.close();
            return nullptr;
        }


        client = new Client(id,firstName,lastName,birthday,address,email,phone);
    }
    else
    {
        fileClient.close();
        fileCont.close();
        return nullptr;
    }

    if (fileCont) {
        std::string id;
        std::string sold;
        std::string activities;
        std::getline(fileCont, id);
        std::getline(fileCont, sold);
        if (id.length() == 0 || sold.length() == 0) {
            std::cout << ">>> One(more) information(s) is(are) incomplete..." << std::endl;
            fileClient.close();
            fileCont.close();
            return nullptr;
        }
        std::vector<Activity> activs;

        while (std::getline(fileCont, activities)) {
            Tokenizer tokenizer;
            Activity activity;
            //std::vector<Activity>activis;
            std::vector<std::string> infoActivs;
            infoActivs = tokenizer.tokenize(activities,'~');
           // for(std::string s :infoActivs)
          //  std::cout<<s<<"!"<<std::endl;
            activity.type = infoActivs.at(0);
            activity.amount = std::stoi(infoActivs.at(1));
            activity.date = infoActivs.at(2);
          //  std::cout<<activity.type<<" "<<activity.amount<<" "<<activity.date<<std::endl;
            activs.push_back(activity);

            }


      //  for(Activity a : activs)
      //      std::cout<<a.type<<" "<<a.amount<<" "<<a.date;

        Cont *cont = new Cont(id,std::stoi(sold),activs);
        client->setCont(cont);
    }
    else
    {
        fileClient.close();
        fileCont.close();
        return nullptr;
    }


     // std::cout<<client.getId()<<" "<<client.getFirstName()<<" "<<client.getLastName()<<" "<<client.getBirthday()<<" "<<client.getAddress()<<" "<<client.getEmail()<<" "<<client.getPhone()<<std::endl;

     //   std::cout<<cont.getId()<<" "<<cont.getSold()<<" "<<std::endl;


    fileClient.close();
    fileCont.close();

    return client;
}


bool ReadWrite::addToSold(Cont *cont, int sum)
{
    std::string id;
    int sold;
    Activity activity;
    std::vector<Activity> activities;
    time_t now = time(0);
    tm* localtm = localtime(&now);
    int i=0;
    id = cont->getId();
    sold = cont->getSold();
    sold = sold + sum;
    activity.type = "Depunere";
    activity.amount = sum;
    activity.date = asctime(localtm);
    activities = cont->getActivities();
    activities.emplace_back(activity);

    lockContFile.lock();
    std::ofstream fileCont("../Database/Cont/" + id);

    if(fileCont){
        fileCont<<id<<std::endl<<sold<<std::endl;
        for(Activity a : activities)
        {   i++;
            fileCont<<a.type<<"~"<<a.amount<<"~"<<a.date;
            if(i != activities.size())
                fileCont<<std::endl;
        }

        std::cout<<">>> Add successfully completed!"<<std::endl;
        fileCont.close();
        lockContFile.unlock();
        return true;
    } else {
        lockContFile.unlock();
        return false;
    }
}

bool ReadWrite:: deleteFromSold(Cont *cont, int sum)
{
    std::string id;
    int sold;
    Activity activity;
    std::vector<Activity> activities;
    time_t now = time(0);
    tm* localtm = localtime(&now);
    int i=0;
    id = cont->getId();
    sold = cont->getSold();

    if(sold - sum >= 0) {
        sold = sold - sum;
        activity.type = "Retragere";
        activity.amount = sum;
        activity.date = asctime(localtm);
        activities = cont->getActivities();
        activities.emplace_back(activity);

        lockContFile.lock();
        std::ofstream fileCont("../Database/Cont/" + id);
        if(fileCont){
            fileCont << id << std::endl << sold << std::endl;
            for (Activity a : activities) {
                i++;
                fileCont << a.type << "~" << a.amount << "~" << a.date;
                if (i != activities.size())
                    fileCont << std::endl;
            }
            std::cout<<">>> Retrieve successfully completed!"<<std::endl;
            fileCont.close();
            lockContFile.unlock();
            return true;
        }
        else {
            lockContFile.unlock();
            return false;
        }
    }
    else{
        std::cout<<">>> Not enough founds!";
        return false;
    }


}