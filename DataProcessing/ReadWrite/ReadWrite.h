//
// Created by cristiana on 16.06.2018.
//

#ifndef ATM_CONCURRENCY_READWRITE_H
#define ATM_CONCURRENCY_READWRITE_H

#include <fstream>
#include <iostream>
#include <vector>
#include <mutex>
#include "../Client/Client.h"
#include "../Cont/Cont.h"
#include "../../core/tokenizer/Tokenizer.h"


class ReadWrite {

    static std::mutex lockContFile;

public:
    struct Card{
        std::string pin;
        std::string serial;
        std::string idCont;
        std::string idClient;
    };

    static Client* processMainFile(std::string introPin, std::string introSerial);
    static bool addToSold(Cont *cont, int sum);
    static bool deleteFromSold(Cont *cont, int sum);
    ReadWrite();
private:
    static Client* processInfoCards(Card cards);


};


#endif //ATM_CONCURRENCY_READWRITE_H
