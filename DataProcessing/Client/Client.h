//
// Created by cristiana on 15.06.2018.
//

#ifndef ATM_CONCURRENCY_CLIENT_H
#define ATM_CONCURRENCY_CLIENT_H

#include<iostream>
#include "../Cont/Cont.h"

class Client {

public:
    Client();
    Client(std::string id, std::string firstName, std::string lastName, std::string birthday, std::string address, std::string email, std::string phone);
    Client(const Client&);
    ~Client();

private:
    std::string id;
    std::string firstName;
    std::string lastName;
    std::string birthday;
    std::string address;
    std::string email;
    std::string phone;
    Cont *cont;

public:
    void setId(std::string id);
    void setFirstName(std::string firstName);
    void setLastName(std::string lastName);
    void setBirthday(std::string birthday);
    void setAddress(std::string address);
    void setEmail(std::string email);
    void setPhone(std::string phone);
    void setCont(Cont *cont);
    std::string getId();
    std::string getFirstName();
    std::string getLastName();
    std::string getBirthday();
    std::string getAddress();
    std::string getEmail();
    std::string getPhone();
    Cont* getCont();
    bool operator==(const Client& client);
};


#endif //ATM_CONCURRENCY_CLIENT_H
