//
// Created by cristiana on 15.06.2018.
//

#include "Client.h"

Client::Client(){}

Client::Client(std::string id, std::string firstName, std::string lastName, std::string birthday, std::string address,
               std::string email, std::string phone) {
    this->id = id;
    this->firstName = firstName;
    this->lastName = lastName;
    this->address = address;
    this->birthday = birthday;
    this->email = email;
    this->phone = phone;
}

Client::Client(const Client &client) {
    this->id = client.id;
    this->firstName = client.firstName;
    this->lastName = client.lastName;
    this->address = client.address;
    this->birthday = client.birthday;
    this->email = client.email;
    this->phone = client.phone;
    this->cont = client.cont;
}

Client::~Client() {}

void Client::setId(std::string id) {
    this->id = id;
}

void Client::setFirstName(std::string firstName) {
    this->firstName = firstName;
}

void Client::setLastName(std::string lastName) {
    this->lastName = lastName;
}

void Client::setAddress(std::string address) {
    this->address = address;
}

void Client::setBirthday(std::string birthday) {
    this->birthday = birthday;
}

void Client::setEmail(std::string email) {
    this->email = email;
}

void Client::setPhone(std::string phone) {
    this->phone = phone;
}

void Client::setCont(Cont *cont) {
    this->cont = cont;
}

std::string Client::getId(){
    return this->id;
}

std::string Client::getFirstName(){
    return this->firstName;
}

std::string Client::getLastName(){
    return this->lastName;
}

std::string Client::getAddress(){
    return this->address;
}

std::string Client::getBirthday(){
    return this->birthday;
}

std::string Client::getEmail(){
    return this->email;
}

std::string Client::getPhone(){
    return this->phone;
}

Cont *Client::getCont(){
    return this->cont;

}

bool Client::operator==(const Client& client)
{
    return this->id == client.id && this->firstName == client.firstName && this->lastName == client.lastName && this->birthday == client.birthday && this->address == client.address && this->email == client.email && this->phone == client.phone && *(this->cont) == *(client.cont);

}
