//
// Created by cristiana on 15.06.2018.
//

#include "Cont.h"


Cont::Cont(){}

Cont::Cont(std::string id, int sold, std::vector<Activity> activities) {
    this->id = id;
    this->sold = sold;
    this->activities = activities;
}

Cont::Cont(const Cont &cont) {
    this->id = cont.id;
    this->sold = cont.sold;
    this->activities = cont.activities;
}

Cont::~Cont() {}

void Cont::setId(std::string id) {
    this->id = id;
}

void Cont::setSold(int sold) {
    this->sold = sold;
}

void Cont::setActivities(std::vector<Activity> activities) {
    this->activities = activities;
}


std::string Cont::getId(){
    return this->id;
}

int Cont::getSold(){
    return this->sold;
}

std::vector<Activity> Cont::getActivities(){
    return this->activities;
}

bool Cont::isEqual(std::vector<Activity> activities1,std::vector<Activity> activities2)
{
    if(activities1.size()!=activities2.size())
        return 0;
    else
    for(int i=0; i<activities1.size(); i++){
        if(!(activities1.at(i).type == activities2.at(i).type && activities1.at(i).amount == activities2.at(i).amount && activities1.at(i).date == activities2.at(i).date))
            return 0;

    }
    return 1;
}

bool Cont::operator==(const Cont& cont)
{
    return this->id == cont.id && this->sold == cont.sold && isEqual(this->activities, cont.activities);
}