//
// Created by cristiana on 15.06.2018.
//

#ifndef ATM_CONCURRENCY_CONT_H
#define ATM_CONCURRENCY_CONT_H

#include<iostream>
#include <vector>


struct Activity{
    std::string type;
    int amount;
    std::string date;
};

class Cont {


public:
    Cont();
    Cont(std::string id, int sold, std::vector<Activity> activities);
    Cont(const Cont&);
    ~Cont();

private:
    std::string id;
    int sold;
    std::vector<Activity> activities;


public:
    void setId(std::string id);
    void setSold(int sold);
    void setActivities(std::vector<Activity> activities);
    std::string getId();
    int getSold();
    std::vector<Activity> getActivities();
    bool operator==(const Cont& cont);
    bool isEqual(std::vector<Activity> activities1,std::vector<Activity> activities2);
};


#endif //ATM_CONCURRENCY_CONT_H
