#include "MessageProcessing.h"

MessageProcessing::MessageProcessing() = default;

MessageProcessing::~MessageProcessing() = default;

MessageProcessing::Message MessageProcessing::fromString(std::string message) {
    if(message == "LOGIN") return LOGIN;
    if(message == "ADD") return ADD;
    if(message == "RET") return RET;
    if(message == "INFO") return INFO;
    if(message == "LOGOUT") return LOGOUT;
    return UNKNOWN;
}

std::string MessageProcessing::toString(MessageProcessing::Message message) {
    switch (message) {
        case LOGIN:
            return "LOGIN";
        case ADD:
            return "ADD";
        case RET:
            return "RET";
        case INFO:
            return "INFO";
        case LOGOUT:
            return "LOGOUT";
        default:
            return "";
    }
}