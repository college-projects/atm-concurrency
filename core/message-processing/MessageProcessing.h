#ifndef ATM_CONCURRENCY_MESSAGEPROCESSING_H
#define ATM_CONCURRENCY_MESSAGEPROCESSING_H

#include <string>

class MessageProcessing {
    public:
        static const char separator = '@';
        enum Message {LOGIN, ADD, RET, INFO, LOGOUT, UNKNOWN};
        MessageProcessing();
        ~MessageProcessing();
        static Message fromString(std::string message);
        static std::string toString(Message message);
};


#endif //ATM_CONCURRENCY_MESSAGEPROCESSING_H
