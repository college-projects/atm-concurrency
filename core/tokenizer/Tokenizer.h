#ifndef ATM_CONCURRENCY_TOKENIZER_H
#define ATM_CONCURRENCY_TOKENIZER_H

#include <iostream>
#include <vector>

class Tokenizer
{
public:
    Tokenizer();
    ~Tokenizer();
    static std::vector<std::string> tokenize(std::string str, char delimiter);
};

#endif //ATM_CONCURRENCY_TOKENIZER_H
