//
// Created by karaaslan on 16.06.2018.
//

#ifndef ATM_CONCURRENCY_ACTIUNICLIENTI_H
#define ATM_CONCURRENCY_ACTIUNICLIENTI_H

#include <string>
#include <utility>
#include <vector>
#include "../Socket/BankConnection.h"
#include "../../core/tokenizer/Tokenizer.h"
#include "../../core/message-processing/MessageProcessing.h"

using namespace std;

class ClientActions {

private:
    BankConnection *bank;
    bool loggedIn;

protected:
    string id_client;
    string id_account;

public:

    ClientActions();
    ~ClientActions();

    bool verifyCredentials(string serial_number, string pin);
    bool addMoney(string ammount);
    bool retrieveMoney(string ammount);
    vector<string> getAccountInfo();
    bool verifyOwner(string client, string account);
    void logOut();
    bool isLoggedIn();
};


#endif //ATM_CONCURRENCY_ACTIUNICLIENTI_H
