//
// Created by karaaslan on 18.06.2018.
//

#include "Menu.h"

void Menu::printBanner()
{
    //Banner
    cout<<"           _._._                       _._._"<<endl;
    cout<<"          _|   |_                     _|   |"<<endl;
    cout<<"          | ... |_._._._._._._._._._._| ... |"<<endl;
    cout<<"          | ||| |  o NATIONAL BANK o  | ||| |"<<endl;
    cout<<"          | \"\"\" |  \"\"\"    \"\"\"    \"\"\"  | \"\"\" |"<<endl;
    cout<<"     ())  |[-|-]| [-|-]  [-|-]  [-|-] |[-|-]|  ())"<<endl;
    cout<<"    (())) |     |---------------------|     | (()))"<<endl;
    cout<<"   (())())| \"\"\" |  \"\"\"    \"\"\"    \"\"\"  | \"\"\" |(())())"<<endl;
    cout<<"   (()))()|[-|-]|  :::   .-\"-.   :::  |[-|-]|(()))()"<<endl;
    cout<<"   ()))(()|     | |~|~|  |_|_|  |~|~| |     |()))(()"<<endl;
    cout<<"      ||  |_____|_|_|_|__|_|_|__|_|_|_|_____|  ||"<<endl;
    cout<<"   ~ ~^^ @@@@@@@@@@@@@@/=======\\@@@@@@@@@@@@@@ ^^~ ~"<<endl<<endl<<endl<<endl;
}

bool Menu::printMainMenu()
{
    string serial;
    string pin;

    printBanner();

    //Main menu
    cout<<"      Welcome to the National Bank, dear customer!"<<endl;
    cout<<" Please enter your credentials for the desired account"<<endl<<endl;
    cout<<"           Serial number: ";
    cin>>serial;
    cout<<endl<<"           Pin:           ";
    cin>>pin;

    //system("clear");

    if(action.verifyCredentials(serial,pin))
        return true;
    else
        return false;
}

void Menu::grantAccess()
{
    int choice=0;
    string amount;
    string::size_type size;
    vector<string> response;

    printBanner();
    cout<<"                       Welcome! "<<endl;
    cout<<"                 How can I assist you?"<<endl;
    cout<<"          1. Retrive money from the account"<<endl;
    cout<<"          2. Add money to the account"<<endl;
    cout<<"          3. See hystory about the account"<<endl;
    cout<<"          4. Exit"<<endl;

    READCHOICE:
    cin>>choice;
    //system("clear");

    switch(choice)
    {
        case 1:
            printBanner();

            ENTER_AMOUNT_RET:
            cout<<"                Enter the ammount (multiple of 5): "<<endl;
            cin>>amount;

            if(stoi(amount,&size) % 5 != 0)
            {
                cout<<"                       Invalid ammount!"<<endl;
                goto ENTER_AMOUNT_RET;
            }

            if(action.retrieveMoney(amount))
            {
                //system("clear");

                printBanner();
                cout << "                   Transaction succeded!"<<endl;
            }
            else
            {
                cout<<"                     Something went wrong!"<<endl;
            }

            break;
        case 2:
            printBanner();

            ENTER_AMOUNT_ADD:
            cout<<"                Enter the ammount (multiple of 5): "<<endl;
            cin>>amount;

            if(stoi(amount,&size) % 10 != 0)
            {
                cout<<"                       Invalid ammount!"<<endl;
                goto ENTER_AMOUNT_ADD;
            }

            if(action.addMoney(amount))
            {
                //system("clear");

                printBanner();
                cout << "                   Transaction succeded!"<<endl;
            }
            else
            {
                cout<<"                     Something went wrong!"<<endl;
            }

            break;
        case 3:
            printBanner();

            response = action.getAccountInfo();

            if(response.size() > 0)
            {
                for(int cnt=0; cnt<response.size(); cnt++)
                    cout<<response[cnt]<<endl;
            }
            else
            {
                cout<<"              Something went wrong!"<<endl;
            }

            break;
        case 4:
            cout<<"               Thank you for choosing us!"<<endl;
            cout<<"               Have a nice day (or night)!"<<endl;

            action.logOut();
            break;
        default:
            cin.clear();
            cin.ignore(100, '\n');
            cout<<"    Invalid option. Please try again    "<<endl;
            goto READCHOICE;
    }

    //system("clear");
}

void Menu::rejectAccess()
{
    printBanner();
    cout<<"              Serial and/or pin is wrong."<<endl;
    cout<<"                  Please try again."<<endl<<endl<<endl;

    //system("clear");
}

void Menu::startATM()
{
    unsigned short tries = 0;

    while(printMainMenu() == false && tries < 3)
    {
        rejectAccess();
        tries++;
    }
    if(tries < 3)
        grantAccess();
    else
    {
        printBanner();
        cout<<"                You shall not pass!!!!"<<endl;
        this_thread::sleep_for (chrono::seconds(1));
        cout<<"                 Police was notified!"<<endl;
        this_thread::sleep_for (chrono::seconds(1));
        cout<<"              Thank you for choosing us!"<<endl;
        this_thread::sleep_for (chrono::seconds(1));
        cout<<"                   Have a nice day!"<<endl;
    }
}