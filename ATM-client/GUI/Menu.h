//
// Created by karaaslan on 18.06.2018.
//

#ifndef ATM_CONCURRENCY_MENU_H
#define ATM_CONCURRENCY_MENU_H

#include "ClientActions.h"
#include <iostream>
#include <stdlib.h>
#include <limits>
#include <thread>

class Menu {

    ClientActions action;

    void printBanner();
    bool printMainMenu();
    void grantAccess();
    void rejectAccess();

public:
    void startATM();

};


#endif //ATM_CONCURRENCY_MENU_H
