//
// Created by karaaslan on 16.06.2018.
//

#include "ClientActions.h"

ClientActions::ClientActions()
{
    bank = new BankConnection();
    bank->openConnection();
    loggedIn = false;
};

ClientActions::~ClientActions()
{
    bank->closeConection();
    delete bank;
    bank = nullptr;
};

bool ClientActions::verifyCredentials(string serial_number, string pin)
{
    if(bank->socketCreated() == false)
        return false;

    if(bank->connectionAvailable() == false)
        if(bank->openConnection() == false)
            return false;

    string response;

    if(bank->sendMessage(MessageProcessing::toString(MessageProcessing::LOGIN) +
                         MessageProcessing::separator +
                         serial_number +
                         MessageProcessing::separator +
                         pin) == false)
    {
        bank->closeConection();
        return false;
    }
    if((response = bank->receiveMessage()).empty())
    {
        bank->closeConection();
        return false;
    }

    vector<string> tokenizedResponse = Tokenizer::tokenize(response,MessageProcessing::separator);

    if(tokenizedResponse[2].compare("0") == 0)
        return false;

    id_client.assign(tokenizedResponse[0]);
    id_account.assign(tokenizedResponse[1]);

    loggedIn=true;

    return true;
};

bool ClientActions::addMoney(string ammount)
{
    if(bank->socketCreated() == false)
        return false;

    if(bank->connectionAvailable() == false)
        if(bank->openConnection() == false)
            return false;

    string response;

    if(bank->sendMessage(MessageProcessing::toString(MessageProcessing::ADD) +
                         MessageProcessing::separator +
                         id_client +
                         MessageProcessing::separator +
                         id_account +
                         MessageProcessing::separator +
                         ammount) == false)
    {
        bank->closeConection();
        return false;
    }
    if((response = bank->receiveMessage()).empty())
    {
        bank->closeConection();
        return false;
    }

    vector<string> tokenizedResponse = Tokenizer::tokenize(response,MessageProcessing::separator);

    if(verifyOwner(tokenizedResponse[0],tokenizedResponse[1]) == false)
        return false;

    if(tokenizedResponse[2].compare("0") == 0)
        return false;

    logOut();

    return true;
};

bool ClientActions::retrieveMoney(string ammount)
{
    if(bank->socketCreated() == false)
        return false;

    if(bank->connectionAvailable() == false)
        if(bank->openConnection() == false)
            return false;

    string response;

    if(bank->sendMessage(MessageProcessing::toString(MessageProcessing::RET) +
                         MessageProcessing::separator +
                         id_client +
                         MessageProcessing::separator +
                         id_account +
                         MessageProcessing::separator +
                         ammount) == false)
    {
        bank->closeConection();
        return false;
    }
    if((response = bank->receiveMessage()).empty())
    {
        bank->closeConection();
        return false;
    }

    vector<string> tokenizedResponse = Tokenizer::tokenize(response,MessageProcessing::separator);

    if(verifyOwner(tokenizedResponse[0],tokenizedResponse[1]) == false)
        return false;

    if(tokenizedResponse[2].compare("0") == 0)
        return false;

    logOut();

    return true;
};

vector<string> ClientActions::getAccountInfo()
{
    bool status = true;
    vector <string> infoLines;
    vector<string> tokenizedResponse;
    string response;

    if(bank->socketCreated() == false)
    {
        status = false;
        goto CREATE_PAIR;
    }

    if(bank->connectionAvailable() == false)
        if(bank->openConnection() == false)
        {
            status = false;
            goto CREATE_PAIR;
        }

    if(bank->sendMessage(MessageProcessing::toString(MessageProcessing::INFO) +
                         MessageProcessing::separator +
                         id_client +
                         MessageProcessing::separator +
                         id_account) == false)
    {
        status = false;
        goto CREATE_PAIR;
    }
    if ((response = bank->receiveMessage()).empty())
    {
        status = false;
        goto CREATE_PAIR;
    }

    tokenizedResponse = Tokenizer::tokenize(response, MessageProcessing::separator);

    if(verifyOwner(tokenizedResponse[0],tokenizedResponse[1]) == false)
    {
        status = false;
        goto CREATE_PAIR;
    }

    for(int cnt = 2; cnt < tokenizedResponse.size(); cnt++)
        infoLines.push_back(tokenizedResponse[cnt]);

    CREATE_PAIR:
    logOut();

    return infoLines;
};

bool ClientActions::verifyOwner(string client, string account)
{
    if(client.compare(id_client) == 0 && account.compare(id_account) == 0)
        return true;
    return false;
}

bool ClientActions::isLoggedIn()
{
    return loggedIn;
}

void ClientActions::logOut()
{
    string response;

    if(bank->socketCreated() == false)
    {
        if(bank->connectionAvailable() == false)
            if(bank->openConnection() == false);
                goto LOG_OUT_END;
    }

    if(bank->sendMessage(MessageProcessing::toString(MessageProcessing::LOGOUT) +
                         MessageProcessing::separator +
                         id_client +
                         MessageProcessing::separator +
                         id_account) == false)
    {
        goto LOG_OUT_END;
    }

    response = bank->receiveMessage();

    LOG_OUT_END:
    bank->closeConection();
}