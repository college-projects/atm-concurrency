//
// Created by karaaslan on 16.06.2018.
//

#ifndef ATM_CONCURRENCY_BANKCONNECTION_H
#define ATM_CONCURRENCY_BANKCONNECTION_H

#include <string>
#include <netinet/in.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <strings.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <cstring>

using namespace std;

class BankConnection {

private:
    int sockfd, portno;

    struct sockaddr_in serv_addr;

    bool connectionEstablished;

public:

    BankConnection(int port = 3000);

    bool openConnection();
    bool sendMessage(string message);
    string receiveMessage();
    bool closeConection();
    inline bool connectionAvailable();
    inline bool socketCreated();
};

inline bool BankConnection::connectionAvailable()
{
    return connectionEstablished;
};

inline bool BankConnection::socketCreated()
{
    if(sockfd == -1)
        return false;
    return true;
};


#endif //ATM_CONCURRENCY_BANKCONNECTION_H
