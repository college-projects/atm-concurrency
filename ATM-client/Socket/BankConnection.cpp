//
// Created by karaaslan on 16.06.2018.
//

#include "BankConnection.h"

BankConnection::BankConnection(int port):portno(port)
{
    connectionEstablished = false;

    sockfd = socket(AF_INET, SOCK_STREAM, 0);

    bzero((char *) &serv_addr, sizeof(serv_addr));

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
    serv_addr.sin_port = htons(portno);
};

bool BankConnection::openConnection()
{
    if(sockfd == -1)
        BankConnection();

    if (sockfd==-1 || connect(sockfd,(struct sockaddr *)&serv_addr,sizeof(serv_addr)) < 0)
        return false;

    connectionEstablished = true;

    return true;
};

bool BankConnection::closeConection()
{
    if(sockfd == -1)
        goto SKIP;
    if (shutdown(sockfd,2) == -1)
        return false;

    sockfd = -1;

    SKIP:
        connectionEstablished = false;

    return true;
};

bool BankConnection::sendMessage(string message)
{
    if(connectionEstablished == false)
        return false;

    int written = 0;

    written = send(sockfd,message.c_str(),100000,0);

    if (written < 0)
        return false;
    return true;
};

string BankConnection::receiveMessage()
{
    if(connectionEstablished == false)
        return "";

    int readen = 0;

    char temp[100000];
    bzero(temp,100000);

    readen = recv(sockfd,temp,sizeof(temp),0);

    if (readen < 0)
        return "";

    string response(temp);

    return response;
};
