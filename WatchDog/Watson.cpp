//
// Created by karaaslan on 21.06.2018.
//

#include "Watson.h"

DataStructure * Watson::sharedData = DataStructure::getInstance();

void Watson::startWatch()
{
    int status = pthread_create(&thread, NULL, analizeAccounts, NULL);
    if (status){
        // current date/time based on current system
        time_t now = time(0);
        // convert now to string form
        char* log_time = ctime(&now);

        cout << log_time << ">>>Watchdog could not been started! Please call maintenance!" << endl;
    }
}

bool Watson::operationAllowed(time_t activity)
{
    double dif = difftime (time(0), activity);

    if(dif > time_threashold)
        return true;

    return false;
}

void* Watson::analizeAccounts(void *argument)
{
    struct timespec sleep_time;
    sleep_time.tv_nsec=0;
    sleep_time.tv_sec=120;
    time_t now;
    char* log_time;

    while(true)
    {
        now = time(0);
        log_time = ctime(&now);
        sharedData->cleanRecords();
        cout << log_time << ">>> Watchdog started to clean records..." << endl;
        sharedData->cleanClients();
        cout << log_time << ">>> Watchdog started to clean clients..." << endl;

        nanosleep(&sleep_time , NULL);
    }
}