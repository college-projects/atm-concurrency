//
// Created by karaaslan on 21.06.2018.
//

#ifndef ATM_CONCURRENCY_WATSON_H
#define ATM_CONCURRENCY_WATSON_H

#include <pthread.h>
#include <iostream>
#include <time.h>
#include <vector>
#include "../bank/data-structure/DataStructure.h"

using namespace std;

class Watson {

    pthread_t thread;
    static const int time_threashold = 120;

    static void* analizeAccounts(void *argument);

public:
    static DataStructure * sharedData;

    void startWatch();
    static bool operationAllowed(time_t activity);

};


#endif //ATM_CONCURRENCY_WATSON_H
