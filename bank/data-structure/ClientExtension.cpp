#include "ClientExtension.h"
#include "../../WatchDog/Watson.h"

ClientExtension::ClientExtension() = default;

ClientExtension::ClientExtension(const ClientExtension& clientExtension) {
    this->client = clientExtension.client;
    this->accountDetails = clientExtension.accountDetails;
    this->accountRecords = clientExtension.accountRecords;
}

ClientExtension::~ClientExtension() = default;

void ClientExtension::setClient(Client *value) {
    this->client = value;
}

Client* ClientExtension::getClient() {
    return this->client;
}

bool ClientExtension::cleanUpClient() {
    try {
        this->client = nullptr;
    } catch (error_t) {
        return false;
    }
    return true;
}

bool ClientExtension::updatePersistentAmount(int difference) {
    if (this->client == nullptr) return false;
    if (this->client->getCont() == nullptr) return false;
    if (this->accountDetails.find(this->client->getCont()->getId()) == this->accountDetails.end()) return false;
    this->accountDetails[this->client->getCont()->getId()] += difference;

    std::pair<time_t,int> record;
    record.first = time(0);
    record.second = difference;
    this->accountRecords[this->client->getCont()->getId()].push_back(record);
    return true;
}

bool ClientExtension::updateClientAmount(int difference) {
    if (this->client == nullptr) return false;
    if (this->client->getCont() == nullptr) return false;
    int newBalance = this->client->getCont()->getSold() + difference;
    Activity activity;
    time_t now = time(0);
    tm* localtm = localtime(&now);
    if(difference < 0) {
        activity.type = "Retragere";
        activity.amount = difference * -1;
    } else {
        activity.type = "Depunere";
        activity.amount = difference;
    }
    activity.date = asctime(localtm);
    if(!activity.date.empty()) activity.date = activity.date.substr(0, activity.date.size()-1);
    std::vector<Activity> activities = this->client->getCont()->getActivities();
    activities.emplace_back(activity);
    this->client->getCont()->setSold(newBalance);
    this->client->getCont()->setActivities(activities);
    return true;
}

bool ClientExtension::canUpdate(int amountToGet) {
    if (this->client == nullptr) return false;
    if (this->client->getCont() == nullptr) return false;
    if (this->accountDetails.find(this->client->getCont()->getId()) == this->accountDetails.end()) return false;
    int newPersistentSum = this->accountDetails[this->client->getCont()->getId()] + amountToGet;
    return this->client->getCont()->getSold() >= amountToGet && newPersistentSum <= 2000;
}

bool ClientExtension::addDetails() {
    if (this->client == nullptr) return false;
    if (this->client->getCont() == nullptr) return false;
    this->accountDetails[this->client->getCont()->getId()] = 0;

    std::vector<std::pair<time_t,int>> mapElement;
    this->accountRecords[this->client->getCont()->getId()] = mapElement;

    return true;
}


bool ClientExtension::detailsExist() {
    return this->accountDetails.find(this->client->getCont()->getId()) != this->accountDetails.end();
}

void ClientExtension::removeOldRecords()
{
    for (auto const& record : accountRecords)
    {
        for(int cnt=0; cnt < record.second.size(); cnt++)
        {
            if(Watson::operationAllowed(record.second[cnt].first) == true)
            {
                accountDetails[record.first] -= record.second[cnt].second;
                accountRecords[record.first].erase(accountRecords[record.first].begin() + cnt);
            }
        }
    }
}

void ClientExtension::removeInvalidClients()
{

    for (auto const& record : accountRecords)
    {
        if(record.second.size() == 0)
        {
            accountRecords.erase(record.first);
            accountDetails.erase(record.first);
        }
    }
}