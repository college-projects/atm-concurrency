#include "DataStructure.h"

DataStructure* DataStructure::instance = nullptr;

DataStructure::DataStructure() {
    mutex = PTHREAD_MUTEX_INITIALIZER;
    cond = PTHREAD_COND_INITIALIZER;
};

DataStructure::~DataStructure() = default;

DataStructure* DataStructure::getInstance() {
    if (instance == nullptr) {
        instance = new DataStructure();
    }
    return instance;
}

void DataStructure::deleteInstance() {
    instance = nullptr;
}

bool DataStructure::clientExists(std::string id) {
    pthread_mutex_lock(&mutex);
    while (writeRequests)
        pthread_cond_wait(&cond, &mutex);
    readInProgress++;
    pthread_mutex_unlock(&mutex);
    bool exists = data.find(id) != data.end();
    pthread_mutex_lock(&mutex);
    readInProgress--;
    pthread_cond_broadcast(&cond);
    pthread_mutex_unlock(&mutex);
    return exists;
}

bool DataStructure::clientReferenceExists(std::string id) {
    if(!clientExists(id)) return false;
    pthread_mutex_lock(&mutex);
    while (writeRequests)
        pthread_cond_wait(&cond, &mutex);
    readInProgress++;
    pthread_mutex_unlock(&mutex);
    bool refExists = this->data[id]->getClient() != nullptr;
    pthread_mutex_lock(&mutex);
    readInProgress--;
    pthread_cond_broadcast(&cond);
    pthread_mutex_unlock(&mutex);
    return refExists;
}

bool DataStructure::addClient(std::string id, Client *client) {
    pthread_mutex_lock(&mutex);
    writeRequests++;
    while (readInProgress || writeInProgress) {
        pthread_cond_wait(&cond, &mutex);
    }
    writeInProgress++;
    pthread_mutex_unlock(&mutex);
    ClientExtension *newClient = new ClientExtension();
    newClient->setClient(client);
    if(!newClient->addDetails()) {
        pthread_mutex_lock(&mutex);
        writeInProgress--;
        writeRequests--;
        pthread_cond_broadcast(&cond);
        pthread_mutex_unlock(&mutex);
        return false;
    }
    data[id] = newClient;
    pthread_mutex_lock(&mutex);
    writeInProgress--;
    writeRequests--;
    pthread_cond_broadcast(&cond);
    pthread_mutex_unlock(&mutex);
    return true;
}


bool DataStructure::updateClient(std::string id, Client *client) {
    if(!clientExists(id)) return false;
    pthread_mutex_lock(&mutex);
    writeRequests++;
    while (readInProgress || writeInProgress) {
        pthread_cond_wait(&cond, &mutex);
    }
    writeInProgress++;
    pthread_mutex_unlock(&mutex);
    data[id]->setClient(client);
    if (!data[id]->detailsExist()) {
        data[id]->addDetails();
    }
    pthread_mutex_lock(&mutex);
    writeInProgress--;
    writeRequests--;
    pthread_cond_broadcast(&cond);
    pthread_mutex_unlock(&mutex);
    return true;
}

bool DataStructure::updateClientData(std::string clientId, int sum, bool isAdd) {
    if(!clientExists(clientId)) return false;
    pthread_mutex_lock(&mutex);
    writeRequests++;
    while (readInProgress || writeInProgress) {
        pthread_cond_wait(&cond, &mutex);
    }
    writeInProgress++;
    pthread_mutex_unlock(&mutex);
    if(isAdd) {
        if(!ReadWrite::addToSold(data[clientId]->getClient()->getCont(), sum)) {
            pthread_mutex_lock(&mutex);
            writeInProgress--;
            writeRequests--;
            pthread_cond_broadcast(&cond);
            pthread_mutex_unlock(&mutex);
            return false;
        }
        if(!data[clientId]->updateClientAmount(sum)) {
            pthread_mutex_lock(&mutex);
            writeInProgress--;
            writeRequests--;
            pthread_cond_broadcast(&cond);
            pthread_mutex_unlock(&mutex);
            return false;
        }
    } else {
        if(data[clientId]->canUpdate(sum)) {
            if(!ReadWrite::deleteFromSold(data[clientId]->getClient()->getCont(), sum)) {
                pthread_mutex_lock(&mutex);
                writeInProgress--;
                writeRequests--;
                pthread_cond_broadcast(&cond);
                pthread_mutex_unlock(&mutex);
                return false;
            }
            if(!data[clientId]->updateClientAmount(-1 * sum)) {
                pthread_mutex_lock(&mutex);
                writeInProgress--;
                writeRequests--;
                pthread_cond_broadcast(&cond);
                pthread_mutex_unlock(&mutex);
                return false;
            }
            if(!data[clientId]->updatePersistentAmount(sum)) {
                pthread_mutex_lock(&mutex);
                writeInProgress--;
                writeRequests--;
                pthread_cond_broadcast(&cond);
                pthread_mutex_unlock(&mutex);
                return false;
            }
        } else {
            pthread_mutex_lock(&mutex);
            writeInProgress--;
            writeRequests--;
            pthread_cond_broadcast(&cond);
            pthread_mutex_unlock(&mutex);
            return false;
        }
    }
    pthread_mutex_lock(&mutex);
    writeInProgress--;
    writeRequests--;
    pthread_cond_broadcast(&cond);
    pthread_mutex_unlock(&mutex);
    return true;
}

std::string DataStructure::retrieveInfo(std::string clientId) {
    if(!clientExists(clientId)) return "ERROR";
    pthread_mutex_lock(&mutex);
    while (writeRequests)
        pthread_cond_wait(&cond, &mutex);
    readInProgress++;
    pthread_mutex_unlock(&mutex);
    Cont *clientAccount = data[clientId]->getClient()->getCont();
    if(clientAccount == nullptr) {
        pthread_mutex_lock(&mutex);
        readInProgress--;
        pthread_cond_broadcast(&cond);
        pthread_mutex_unlock(&mutex);
        return "ERROR";
    }
    std::string response = "";
    for(Activity a:clientAccount->getActivities()) {
        response += a.date;
        response += " > ";
        response += a.type;
        response += " -> ";
        response += std::to_string(a.amount);
        response += MessageProcessing::separator;
    }
    if(response.size() > 1) {
        response = response.substr(0, response.size() - 1);
    }
    pthread_mutex_lock(&mutex);
    readInProgress--;
    pthread_cond_broadcast(&cond);
    pthread_mutex_unlock(&mutex);
    return response;
}

void DataStructure::cleanRecords()
{
    pthread_mutex_lock(&mutex);
    writeRequests++;
    while (readInProgress || writeInProgress) {
        pthread_cond_wait(&cond, &mutex);
    }
    writeInProgress++;
    pthread_mutex_unlock(&mutex);
    for (auto const& record : data)
        record.second->removeOldRecords();
    pthread_mutex_lock(&mutex);
    writeInProgress--;
    writeRequests--;
    pthread_cond_broadcast(&cond);
    pthread_mutex_unlock(&mutex);
}

void DataStructure::cleanClients()
{
    pthread_mutex_lock(&mutex);
    writeRequests++;
    while (readInProgress || writeInProgress) {
        pthread_cond_wait(&cond, &mutex);
    }
    writeInProgress++;
    pthread_mutex_unlock(&mutex);
    for (auto const& record : data)
        record.second->removeInvalidClients();
    pthread_mutex_lock(&mutex);
    writeInProgress--;
    writeRequests--;
    pthread_cond_broadcast(&cond);
    pthread_mutex_unlock(&mutex);
}

bool DataStructure::deleteClient(std::string id) {
    if(!clientExists(id)) return false;
    pthread_mutex_lock(&mutex);
    writeRequests++;
    while (readInProgress || writeInProgress) {
        pthread_cond_wait(&cond, &mutex);
    }
    writeInProgress++;
    pthread_mutex_unlock(&mutex);
    bool deleted = data[id]->cleanUpClient();
    pthread_mutex_lock(&mutex);
    writeInProgress--;
    writeRequests--;
    pthread_cond_broadcast(&cond);
    pthread_mutex_unlock(&mutex);
    return deleted;
}