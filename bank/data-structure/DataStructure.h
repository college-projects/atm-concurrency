#ifndef ATM_CONCURRENCY_DATASTRUCTURE_H
#define ATM_CONCURRENCY_DATASTRUCTURE_H

#include <iostream>
#include <string>
#include <map>
#include <pthread.h>
#include "ClientExtension.h"
#include "../../DataProcessing/ReadWrite/ReadWrite.h"
#include "../../DataProcessing/ReadWrite/ReadWrite.h"
#include "../../core/message-processing/MessageProcessing.h"

class DataStructure {
    private:
        static DataStructure *instance;
        std::map<std::string, ClientExtension*> data;
        int writeRequests;
        int writeInProgress;
        int readInProgress;
        pthread_mutex_t mutex;
        pthread_cond_t cond;
    DataStructure();
        ~DataStructure();
    public:
        static DataStructure* getInstance();
        static void deleteInstance();
        bool clientExists(std::string id);
        bool clientReferenceExists(std::string id);
        bool deleteClient(std::string id);
        bool addClient(std::string id, Client *client);
        bool updateClient(std::string id, Client *client);
        bool updateClientData(std::string clientId, int sum, bool isAdd);
        std::string retrieveInfo(std::string clientId);
        void cleanRecords();
        void cleanClients();

};


#endif //ATM_CONCURRENCY_DATASTRUCTURE_H
