#ifndef ATM_CONCURRENCY_CLIENTEXTENSION_H
#define ATM_CONCURRENCY_CLIENTEXTENSION_H


#include "../../DataProcessing/Client/Client.h"
#include <map>
#include <time.h>
#include <vector>
#include <utility>

class ClientExtension {
    private:
        Client *client;
        std::map<std::string, int> accountDetails;
        std::map<std::string, std::vector<std::pair<time_t,int>>> accountRecords;

    public:
        ClientExtension();
        ~ClientExtension();
        ClientExtension(const ClientExtension& clientExtension);
        void setClient(Client *value);
        Client* getClient();
        bool addDetails();
        bool detailsExist();
        bool cleanUpClient();
        bool updatePersistentAmount(int difference);
        bool updateClientAmount(int difference);
        bool canUpdate(int amountToGet);
        void removeOldRecords();
        void removeInvalidClients();
};


#endif //ATM_CONCURRENCY_CLIENTEXTENSION_H
