#ifndef ATM_CONCURRENCY_SOCKETCONNECTION_H
#define ATM_CONCURRENCY_SOCKETCONNECTION_H

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <strings.h>
#include <iostream>
#include "../serve-client/ServeClient.h"

class SocketConnection {
    private:
        int sockfd, portno;
        struct sockaddr_in address;
        ServeClient *serviceProvider;
    public:
        SocketConnection(int port = 3000);
        bool openConnection();
        void waitForClients();
        bool closeConnection();
};


#endif //ATM_CONCURRENCY_SOCKETCONNECTION_H
