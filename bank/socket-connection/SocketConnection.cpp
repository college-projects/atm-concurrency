#include "SocketConnection.h"

SocketConnection::SocketConnection(int port):portno(port) {
    sockfd = socket(AF_INET, SOCK_STREAM, 0);

    bzero((char *) &address, sizeof(address));
    address.sin_family = AF_INET;
    address.sin_port = htons(3000);
    address.sin_addr.s_addr = inet_addr("127.0.0.1");
}

bool SocketConnection::openConnection() {
    if(sockfd == -1)
        SocketConnection();

    if (sockfd == -1 || bind(sockfd, (sockaddr*)&address, sizeof(sockaddr_in)) == -1)
    {
        std::cout << "Bind failed... " << std::endl;
        return false;
    }
    if (sockfd == -1 || listen(sockfd, SOMAXCONN) == -1)
    {
        std::cout << "Listen failed... " << std::endl;
        return false;
    }

    std::cout << "Server started... " << std::endl;
    std::cout << "Listening on: 127.0.0.1:3000 ..." << std::endl;

    return true;
}

void SocketConnection::waitForClients() {
    int client;
    serviceProvider = new ServeClient();
    ServeClient::initSharedData();
    while(client = accept(sockfd, nullptr, nullptr)) {
        if (client == -1) {
            std::cout << sockfd << "Error on accept" << std::endl;
            return;
        }
        serviceProvider->run(client);
    }
}

bool SocketConnection::closeConnection() {
    if(sockfd == -1)
        return true;
    if (shutdown(sockfd,2) == -1)
        return false;
    sockfd = -1;
    return true;
}