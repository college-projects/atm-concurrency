#include <iostream>
#include "data-structure/DataStructure.h"
#include "socket-connection/SocketConnection.h"
#include "../core/message-processing/MessageProcessing.h"
#include "../WatchDog/Watson.h"

int main() {
    Watson watson;
    watson.startWatch();

    SocketConnection *connection = new SocketConnection();
    if (!connection->openConnection()) {
        std::cout << "Failed to start server..." << std::endl;
        return -1;
    }
    connection->waitForClients();
    if (!connection->closeConnection()) {
        std::cout << "Failed to stop server..." << std::endl;
        return -1;
    }
    return 0;
}