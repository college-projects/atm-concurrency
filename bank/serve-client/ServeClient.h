#ifndef ATM_CONCURRENCY_SERVECLIENT_H
#define ATM_CONCURRENCY_SERVECLIENT_H

#include <sys/socket.h>
#include <vector>
#include <string>
#include "../../core/tokenizer/Tokenizer.h"
#include "../../core/message-processing/MessageProcessing.h"
#include "../data-structure/DataStructure.h"
#include "../../DataProcessing/ReadWrite/ReadWrite.h"

class ServeClient {
    private:
        static DataStructure *sharedData;
        static void processLogin(std::string serial, std::string pin, int clientFd);
        static void processAdd(std::string clientId, std::string accountId, int sum, int clientFd);
        static void processRet(std::string clientId, std::string accountId, int sum, int clientFd);
        static void retrieveInfo(std::string clientId, std::string accountId, int clientFd);
        static void processLogout(std::string clientId, std::string accountId, int clientFd);
    public:
        ServeClient();
        static void initSharedData();
        void run(int clientFd);
        static void* serveClient(void* clientFd);
};


#endif //ATM_CONCURRENCY_SERVECLIENT_H
