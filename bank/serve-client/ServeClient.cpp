#include "ServeClient.h"

DataStructure* ServeClient::sharedData = nullptr;

ServeClient::ServeClient() = default;

void ServeClient::initSharedData() {
    sharedData = DataStructure::getInstance();
}

void ServeClient::run(int clientFd) {
    pthread_t threadElement;
    pthread_create(&threadElement, NULL, serveClient, &clientFd);
}

void* ServeClient::serveClient(void *clientFd) {
    int client = *(int*)clientFd;
    std::cout << "Client " << client << " connected..." << std::endl;
    char buffer[100000];
    while (recv(client, &buffer, 100000, 0)) {
        std::string parsedMessage(buffer);
        if(!parsedMessage.empty()) {
            std::vector<std::string> messageTokens = Tokenizer::tokenize(parsedMessage, MessageProcessing::separator);
            switch (MessageProcessing::fromString(messageTokens[0])) {
                case MessageProcessing::LOGIN:
                    processLogin(messageTokens[1], messageTokens[2], client);
                    break;
                case MessageProcessing::ADD:
                    processAdd(messageTokens[1], messageTokens[2], atoi(messageTokens[3].c_str()), client);
                    break;
                case MessageProcessing::RET:
                    processRet(messageTokens[1], messageTokens[2], atoi(messageTokens[3].c_str()), client);
                    break;
                case MessageProcessing::INFO:
                    retrieveInfo(messageTokens[1], messageTokens[2], client);
                    break;
                case MessageProcessing::LOGOUT:
                    processLogout(messageTokens[1], messageTokens[2], client);
                    break;
                case MessageProcessing::UNKNOWN:
                    std::cout << "Unknown message...\n\tContent: ";
                    for (std::string s: messageTokens)
                        std::cout << s;
                    std::cout << std::endl;
                    break;
            }
        }
    }
    std::cout << "Client " << client << " disconnected..." << std::endl;
}

void ServeClient::processLogin(std::string serial, std::string pin, int clientFd) {
    Client *authenticatedClient = ReadWrite::processMainFile(pin, serial);
    std::string buffer;
    std::string noClient = "no_client";
    std::string noCont = "no_cont";
    if (authenticatedClient == nullptr) {
        buffer = noClient +
                 MessageProcessing::separator +
                 noCont +
                 MessageProcessing::separator +
                 "0";
        send(clientFd, buffer.c_str(), 100000, 0);
        return;
    }
    if (sharedData->clientReferenceExists(authenticatedClient->getId())) {
        buffer = noClient +
                 MessageProcessing::separator +
                 noCont +
                 MessageProcessing::separator +
                 "0";
        send(clientFd, buffer.c_str(), 100000, 0);
        return;
    }
    if (!sharedData->clientExists(authenticatedClient->getId())) {
        if (!sharedData->addClient(authenticatedClient->getId(), authenticatedClient)) {
            buffer = noClient +
                     MessageProcessing::separator +
                     noCont +
                     MessageProcessing::separator +
                     "0";
            send(clientFd, buffer.c_str(), 100000, 0);
            return;
        }
    } else {
        if (!sharedData->updateClient(authenticatedClient->getId(), authenticatedClient)) {
            buffer = noClient +
                     MessageProcessing::separator +
                     noCont +
                     MessageProcessing::separator +
                     "0";
            send(clientFd, buffer.c_str(), 100000, 0);
            return;
        }
    }
    buffer = authenticatedClient->getId() +
             MessageProcessing::separator +
             authenticatedClient->getCont()->getId() +
             MessageProcessing::separator +
             "1";
    send(clientFd, buffer.c_str(), 100000, 0);
}

void ServeClient::processAdd(std::string clientId, std::string accountId, int sum, int clientFd) {
    std::string buffer;
    if(!sharedData->clientExists(clientId)) {
        buffer = clientId +
                 MessageProcessing::separator +
                 accountId +
                 MessageProcessing::separator +
                 "0";
        send(clientFd, buffer.c_str(), 100000, 0);
        return;
    }
    if(!sharedData->updateClientData(clientId, sum, true)) {
        buffer = clientId +
                 MessageProcessing::separator +
                 accountId +
                 MessageProcessing::separator +
                 "0";
        send(clientFd, buffer.c_str(), 100000, 0);
        return;
    }
    buffer = clientId +
             MessageProcessing::separator +
             accountId +
             MessageProcessing::separator +
             "1";
    send(clientFd, buffer.c_str(), 100000, 0);
}

void ServeClient::processRet(std::string clientId, std::string accountId, int sum, int clientFd) {
    std::string buffer;
    if(!sharedData->clientExists(clientId)) {
        buffer = clientId +
                 MessageProcessing::separator +
                 accountId +
                 MessageProcessing::separator +
                 "0";
        send(clientFd, buffer.c_str(), 100000, 0);
        return;
    }
    if(!sharedData->updateClientData(clientId, sum, false)) {
        buffer = clientId +
                 MessageProcessing::separator +
                 accountId +
                 MessageProcessing::separator +
                 "0";
        send(clientFd, buffer.c_str(), 100000, 0);
        return;
    }
    buffer = clientId +
             MessageProcessing::separator +
             accountId +
             MessageProcessing::separator +
             "1";
    send(clientFd, buffer.c_str(), 100000, 0);
}

void ServeClient::retrieveInfo(std::string clientId, std::string accountId, int clientFd) {
    std::string buffer;
    if(!sharedData->clientExists(clientId)) {
        buffer = clientId +
                 MessageProcessing::separator +
                 accountId +
                 MessageProcessing::separator +
                 "ERROR";
        send(clientFd, buffer.c_str(), 100000, 0);
        return;
    }
    std::string info = sharedData->retrieveInfo(clientId);
    buffer = clientId +
             MessageProcessing::separator +
             accountId +
             MessageProcessing::separator +
             info;
    send(clientFd, buffer.c_str(), 100000, 0);
}

void ServeClient::processLogout(std::string clientId, std::string accountId, int clientFd) {
    std::string buffer;
    if(!sharedData->clientExists(clientId)) {
        buffer = clientId +
                 MessageProcessing::separator +
                 accountId +
                 MessageProcessing::separator +
                 "0";
        send(clientFd, buffer.c_str(), 100000, 0);
        return;
    }
    if(!sharedData->deleteClient(clientId)) {
        buffer = clientId +
                 MessageProcessing::separator +
                 accountId +
                 MessageProcessing::separator +
                 "0";
        send(clientFd, buffer.c_str(), 100000, 0);
        return;
    }
    buffer = clientId +
             MessageProcessing::separator +
             accountId +
             MessageProcessing::separator +
             "1";
    send(clientFd, buffer.c_str(), 100000, 0);
}